# CarCar

Team:

* Person 1 - Which microservice? - Constance Basco | Sales Microservice
* Person 2 - Which microservice? - Amanda Southern | Service Microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

My Models include the technician model, appointment model, and the automobileVO model. The automobile VO model polls to the inventory microservice to see which automobiles have been sold, and what their VIN is. automobiles that have requested service and have a vin that matches one in inventory will be marked as VIP on the front end. The technician model is a foreign key to the appointment model so that a technician can be assigned to an appointment. This also means that technicians cannot be deleted if they are assigned to a pre existing appointment.

## Sales microservice

My models included Sale, Customer, AutomobileVO and Salesperson. I used the AutomobileVO as a gateway to the automobile in Inventory. In Sale I used foreign key to set up relations with the Customer model and Salesperson model so that I could display Sale information connected to specific salespeople and custoemrs.
